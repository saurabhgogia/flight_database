<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

	<%
	    
		try {
			//Get the database connection
			ApplicationDB db = new ApplicationDB();	
			Connection con = db.getConnection();		

			//Create a SQL statement
			Statement stmt = con.createStatement();
			//Get the selected radio button from the index.jsp
			//Make a SELECT query from the table specified by the 'command' parameter at the index.jsp
			
			int userIDEntered = Integer.parseInt(request.getParameter("userIdPassedToDeleteJSP")); 
			String sql = "DELETE FROM users WHERE userID="+userIDEntered;
			System.out.println(sql);
			//Run the query against the database.
			int result = stmt.executeUpdate(sql);
			System.out.println("Result: "+result);
			response.sendRedirect("/cs336Final/pages/admin/admin.jsp");
			//close the connection.
			db.closeConnection(con);
		} catch (Exception e) {
			out.print(e);
		}
	%>
