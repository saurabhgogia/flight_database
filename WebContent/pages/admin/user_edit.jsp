<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

	<%
	    
		try {
			//PROJECT3
			//Get the database connection
			ApplicationDB db = new ApplicationDB();	
			Connection con = db.getConnection();		

			//Create a SQL statement
			Statement stmt = con.createStatement();
			//Get the selected radio button from the index.jsp
			String userEntered = request.getParameter("userIdPassedToJSP");
			
			//Make a SELECT query from the table specified by the 'command' parameter at the index.jsp
			
			String sql = "Select * from users where userId='" + userEntered+"'";
			//Run the query against the database.
			ResultSet result = stmt.executeQuery(sql);
			result.next();
			int role = result.getInt("role");
			%>
			<!-- Bootstrap CSS -->
		    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<!-- Fontawesome CSS -->
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<body>
		<!--  navigation bar -->
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="#">Admin Console</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link">User Management</a>
		      </li>
		    </ul>
		    <a href="/cs336Final/logout.jsp"">Log Out</a>
		  </div>
		</nav>
		<!--   end navigation bar -->
		<!-- container for entire page styling -->
		<div style="margin-left: 3em; width='100%'">
					<div class="row">
						<h3 class = "col-md-8">User Edit</h3>
						<a class = "col-md-4" href="/cs336Final/pages/admin/admin.jsp">Back</a>
					</div>
					<!--  HTML FORM FOR INPUT -->
					<form method="post" action="user_edit_controller.jsp">
				    <!-- note the show.jsp will be invoked when the choice is made -->
					<!-- The next lines give HTML for radio buttons being displayed -->
					   UserID:			<input type="text" name="userId" style="display:hidden;" value="<%=result.getString("userId") %>"/>
					   <br>
					   Username:		<input type="text" name="username" value="<%=result.getString("username") %>"/> 
					  <br>
					   Password:		<input type="text" name="password" value="<%=result.getString("password")%>"/>
					  <br>
					   Name:			<input type="text" name="name" value="<%=result.getString("name")%>"/>
					  <br>
					   
					   Role:			
					   					<select name = "role">
					   						<option value="0" <%if((result.getInt("role"))==0){%> selected <%}%>>Customer</option>
					   						<option value="1" <%if((result.getInt("role"))==1){%> selected <%}%> >Admin</option>
					   						<option value="2" <%if((result.getInt("role"))==2){%> selected <%}%>>Representative</option>
					   					</select>
					  
				    <!-- when the radio for bars is chosen, then 'command' will have value 
				     | 'bars', in the show.jsp file, when you access request.parameters -->
				  <br>
				  <input type="submit" value="submit"/>
		</form>
		</div>
			
			<% 
			
			//close the connection.
			db.closeConnection(con);
		} catch (Exception e) {
			out.print(e);
		}
	%>
