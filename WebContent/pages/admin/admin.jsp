<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Console</title>
</head>
<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!-- Fontawesome CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
<!--  navigation bar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Admin Console</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link">User Management</a>
      </li>
    </ul>
    <a href="/cs336Final/logout.jsp">Log Out</a>
  </div>
</nav>
<!--   end navigation bar -->
<!-- container for entire page styling -->
<div class = "container" style="margin-left: 3em;">


<!--  start User Management Tab -->
<div class = "userManagement row">
<table border="2" style="width:100%; margin-top: 3em;">
   <tr>
        <td>userID</td>
        <td>Username</td>
        <td>Password</td>
        <td>Name</td>
        <td>Role</td>
        <td>		</td>
   </tr>
   <%
   try
   {
       ApplicationDB db = new ApplicationDB();
       Connection con = db.getConnection();
       String query="select * from users";
       Statement stmt=con.createStatement();
       ResultSet rs=stmt.executeQuery(query);
       while(rs.next())
       {
   %>
   <tr>
   <td><%=rs.getInt("userId")%></td>
   <td><%=rs.getString("username")%></td>
   <td><%=rs.getString("password")%></td>
   <td><%=rs.getString("name")%></td>
   <td><%=rs.getInt("role")%></td>
   <td>
   		<button type="button" class="btn btn-warning" onclick="editUser(<%=rs.getInt("userId")%>);">
   			<i class="fa fa-pencil" ></i>
   		</button>
   		<button type="button" class="btn btn-danger" onclick="deleteUser(<%=rs.getInt("userId")%>);">
			<i class="fa fa-times delete-userId-<%=rs.getInt("userId")%>"></i>
		</button>
   </td>
   </tr>
   
   <%
       }
   %>
   </table>
   
   <form id = "userIdPassingToEditJSP" method="post" action="user_edit.jsp" style="display: none;">
    <!-- note the show.jsp will be invoked when the choice is made -->
	<!-- The next lines give HTML for radio buttons being displayed -->
	  <input type="text" id="userIdPassedToEditJSP" name="userIdPassedToJSP"></input>
    <!-- when the radio for bars is chosen, then 'command' will have value 
     | 'bars', in the show.jsp file, when you access request.parameters -->
  <br>
  <input type="submit" value="submit"/>
</form>
   <form id = "userIdPassingToDeleteJSP" method="post" action="user_delete_controller.jsp" style="display: none;">
    <!-- note the show.jsp will be invoked when the choice is made -->
	<!-- The next lines give HTML for radio buttons being displayed -->
	  <input type="text" id="userIdPassedToDeleteJSP" name="userIdPassedToDeleteJSP"></input>
    <!-- when the radio for bars is chosen, then 'command' will have value 
     | 'bars', in the show.jsp file, when you access request.parameters -->
  <br>
  <input type="submit" value="submit"/>
</form>
   <%
        rs.close();
        stmt.close();
        con.close();
   }
   catch(Exception e)
   {
        e.printStackTrace();
   }
   %>
  
</div>

</div>
</body>
	<!--  custom javascript code -->
	<script src="admin.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>