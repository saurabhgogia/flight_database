<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Flight Search Results: Leg 1</title>
<script src="jquery-3.4.1.min.js"></script>
</head>
<body onload="intializeDropDown(<% request.getParameter("sortbyfieldname"); %>)">
	<select onchange="sortReloadPage(this.value)" name="sortSelect">
	<option value = "none"> none </option>
	<option value = "price"> Price </option>
	<option value = "takeOffTime"> Take-Off Time </option>
	<option value = "landTime"> Landing Time </option>
	</select>
	<br>
	Filter By:<br>
	Price: <input onchange = "filterReloadPage(this.value)" type="text" name="priceFilter"/>
	
	<%
		try {
	
			//Get the database connection
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url = "jdbc:mysql://mydatabase.cd0pmmdd0vk4.us-east-2.rds.amazonaws.com:3306/FlightReservations";
			Connection con = DriverManager.getConnection(url,"admin", "admin123");    
			//String url = "jdbc:mysql://cs336.ckksjtjg2jto.us-east-2.rds.amazonaws.com:3036/FlightReservation";

			//ApplicationDB db = new ApplicationDB();	
			//Connection con = db.getConnection();		

			//Create a SQL statement
			Statement stmt = con.createStatement();
			//Get the selected radio button from the index.jsp
			String ftype = request.getParameter("ftype"); //Flight Type Radio button - if Round Trip, navigate to searchResultsRoundTrip for Leg 2
			String originAirport = request.getParameter("originAirport");
			String destinationAirport = request.getParameter("destinationAirport");
			String deptDate = request.getParameter("deptDate");
			String isFlexible = request.getParameter("isFlexible");
			String sortBy = request.getParameter("sortBy");
			String arrDate = request.getParameter("arrDate");
			
			//Make a SELECT query from the table specified by the 'command' parameter at the index.jsp
			
			String base = "SELECT * FROM routes ";
			String whereClause = " WHERE takeOffAirport='" +originAirport+"' and landAirport='" +destinationAirport+"'";
			String dayOfWeek = " and dayOfWeek like CONCAT(\'%\',CAST(DAYOFWEEK(\'" + deptDate + "\') AS CHAR),\'%')"; 
			// String sortByQuery = " ORDER BY "+sortBy;
			String str;
			//Run the query against the database.
			String priceMax = request.getParameter("priceMax");
			
			if( priceMax != null && !priceMax.isEmpty()){
					whereClause += " and cost < " + priceMax;
					out.print(" Filtering Price Less than "+priceMax);
			}

			
		    
			    if(isFlexible != null && !isFlexible.equals("unchecked")){ //Box is Checked
			    	str = base + whereClause;
			    }
			    else{
			    	str = base + whereClause + dayOfWeek;
			    }
					
			String sortType = request.getParameter("sortType");
			if( sortType != null && !sortType.equals("none")){
				if(sortType.equals("price")){
					str += " order by cost";
				}
				else if(sortType.equals("takeOffTime")){
					str += " order by takeOffTime";
				}
				else if(sortType.equals("landTime")){
					str += " order by landTime";
				}
				out.print(" Sorting Results by " + sortType);
			}
			
			//Execute Str Query
			
			ResultSet result = stmt.executeQuery(str);
			
			//Make an HTML table to show the results in:
			out.print("<table id=\"resultsTable\">");

			//make a row
			out.print("<tr>");
			//First Column = Radio Button for select a flight to reserve
			out.print("<td>");
			out.print("Select Flight?");
			out.print("</td>");
			//make a column
			out.print("<td>");
			//print out column header
			out.print("Route ID");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("Airline ID");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("Aircraft ID");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("Flight Number");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("Take Off Airport");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("Land Airport");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("Take Off Time");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("Land Time");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("Available Economy Seats");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("Available Business Seats");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("isActive?");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("Cost");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("Day(s) of Week in Operation");
			out.print("</td>");
			//make a column
			out.print("<td>");
			out.print("Fleet ID");
			out.print("</td>");
			//depending on the radio button selection make a column header for Manufacturer if the beers table was selected and Address if the bars table was selected

			//parse out the results
			while (result.next()) {
				//make a row for each result entry
				out.print("<tr>");
				//make a column for each field (first is radio button)
				out.print("<td>");
				out.print("<input type=\"radio\" name=\"selectedFlightLeg\" value=\"selected\"/>");
				out.print("</td>");
				
				out.print("<td>");
				//Print out result value for the field
				out.print(result.getString("routeID"));
				out.print("</td>");
				//repeat until all columns included in query are covered
				out.print("<td>");
				out.print(result.getString("airlineID"));
				out.print("</td>");
				out.print("<td>");
				out.print(result.getString("aircraftID"));
				out.print("</td>");
				out.print("<td>");
				out.print(result.getString("flightNum"));
				out.print("</td>");
				out.print("<td>");
				out.print(result.getString("takeOffAirport"));
				out.print("</td>");
				out.print("<td>");
				out.print(result.getString("landAirport"));
				out.print("</td>");
				out.print("<td>");
				out.print(result.getString("takeOffTime"));
				out.print("</td>");
				out.print("<td>");
				out.print(result.getString("landTime"));
				out.print("</td>");
				out.print("<td>");
				out.print(result.getString("availEconSeat"));
				out.print("</td>");
				out.print("<td>");
				out.print(result.getString("availBusinessSeat"));
				out.print("</td>");
				out.print("<td>");
				out.print(result.getString("isActive"));
				out.print("</td>");
				out.print("<td>");
				out.print(result.getString("cost"));
				out.print("</td>");
				out.print("<td>");
				out.print(result.getString("dayOfWeek"));
				out.print("</td>");
				out.print("<td>");
				out.print(result.getString("fleetID"));
				out.print("</td>");

			}
			out.print("</table>");
		if(ftype.equals("roundtrip")){
			out.print("<button onclick = \"secondLegReload()\"> Select Next Leg-> </a>");
		}
		else if(ftype.equals("oneway")){
			out.print("<a href = \"confirmation.jsp\">Confirm -> </a>");
		}
			//close the connection.
			//db.closeConnection(con);
			con.close();
		} catch (Exception e) {
			out.print(e);
		}
	%>
<script>
function sortTable() {
	  var url = window.location.href
		  window.location = url;
	  alert(url);
	  }
</script>
<script>
function sortReloadPage(selectObject) {
	alert('Reload');
	//var selection = selectObject.value;
	//var e = document.getElementById("sortSelect");
	//var sortSelection = e.options[e.selectedIndex].value;
	alert(selectObject);
	var payload = {
			ftype: '<%= request.getParameter("ftype") %>',
			originAirport: '<%= request.getParameter("originAirport") %>',
			destinationAirport:'<%= request.getParameter("destinationAirport") %>',
			deptDate:'<%= request.getParameter("deptDate") %>',
			isFlexible:'<% if(request.getParameter("isFlexible")==null){out.print("unchecked");} else { out.print("checked");} %>',
			sortType:selectObject
	};
	alert(JSON.stringify(payload));
		 const form = document.createElement('form');
		 form.method = 'post';
		 form.action = window.location.href;
		 for ([key, value] of Object.entries(payload)) {
		     
		     const hiddenField = document.createElement('input');
		     hiddenField.type = 'hidden';
		     hiddenField.name = key;
		     hiddenField.value = value;
		     form.appendChild(hiddenField);
		 }
		 document.body.appendChild(form);
		 form.submit();
		}
		
function filterReloadPage(selectObject) {
	alert('Reload');
	//var selection = selectObject.value;
	//var e = document.getElementById("sortSelect");
	//var sortSelection = e.options[e.selectedIndex].value;
	alert(selectObject);
	var payload = {
			ftype: '<%= request.getParameter("ftype") %>',
			originAirport: '<%= request.getParameter("originAirport") %>',
			destinationAirport:'<%= request.getParameter("destinationAirport") %>',
			deptDate:'<%= request.getParameter("deptDate") %>',
			isFlexible:'<% if(request.getParameter("isFlexible")==null){out.print("unchecked");} else { out.print("checked");} %>',
			priceMax:selectObject
	};
	alert(JSON.stringify(payload));
		 const form = document.createElement('form');
		 form.method = 'post';
		 form.action = window.location.href;
		 for ([key, value] of Object.entries(payload)) {
		     const hiddenField = document.createElement('input');
		     hiddenField.type = 'hidden';
		     hiddenField.name = key;
		     hiddenField.value = value;
		     form.appendChild(hiddenField);
		 }
		 document.body.appendChild(form);
		 form.submit();
		}
		
function secondLegReload() {
	alert('Reload');
	//var selection = selectObject.value;
	//var e = document.getElementById("sortSelect");
	//var sortSelection = e.options[e.selectedIndex].value;
	var payload = {
			ftype: '<%= request.getParameter("ftype") %>',
			originAirport: '<%= request.getParameter("destinationAirport") %>',
			destinationAirport:'<%= request.getParameter("originAirport") %>',
			deptDate:'<%= request.getParameter("arrDate") %>',
			isFlexible:'<% if(request.getParameter("isFlexible")==null){out.print("unchecked");} else { out.print("checked");} %>',
			
	};
	alert(JSON.stringify(payload));
		 const form = document.createElement('form');
		 form.method = 'post';
		 form.action = window.location.href;
		 for ([key, value] of Object.entries(payload)) {
		     const hiddenField = document.createElement('input');
		     hiddenField.type = 'hidden';
		     hiddenField.name = key;
		     hiddenField.value = value;
		     form.appendChild(hiddenField);
		 }
		 document.body.appendChild(form);
		 form.submit();
		}
	function initializeDropDown(sortbyfield) {
		document.getElementById('sortdropdownIDstring').value = sortbyfield;
	}
</script>
</body>
</html>