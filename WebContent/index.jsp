<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Page</title>
</head>
<body>


<div style="color:blue">${logOut}</div>
<br>
Register
<hr>

<form method="post" action="register.jsp">
    <!-- note the show.jsp will be invoked when the choice is made -->
	<!-- The next lines give HTML for radio buttons being displayed -->
	  <div style="color:red">${existingUser}</div>
	  <div style="color:green">${userCreated}</div>
	   Username:		<input type="text" name="username" />
	  <br>
	   Password: 		<input type="password" name="password"/>
    <!-- when the radio for bars is chosen, then 'command' will have value 
     | 'bars', in the show.jsp file, when you access request.parameters -->
  <br>
  <input type="submit" value="submit"/>
</form>
<br>

Log In 
<hr>

<form method="post" action="login.jsp">
    <!-- note the show.jsp will be invoked when the choice is made -->
	<!-- The next lines give HTML for radio buttons being displayed -->
	  <div style="color:red">${incorrectLogin}</div>
	
	   Username:		<input type="text" name="username" />
	  <br>
	   Password: 		<input type="password" name="password"/>
    <!-- when the radio for bars is chosen, then 'command' will have value 
     | 'bars', in the show.jsp file, when you access request.parameters -->
  <br>
  <input type="submit" value="submit" />
</form>


</body>
</html>