<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="com.cs336.pkg.*"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title> Welcome</title>
</head>
<body>
<h1 class = "float: left"> Welcome, ${user}! </h1>
<a href = "logout.jsp">Log Out</a>
<br>
<h2>Search For a Flight:</h2>
<h3>Pick a Flight Type:</h3>

<form method="post" action = "customerSearchResults.jsp" id="searchFlight">
	<input type="radio" onclick="DateEnable()" name = "ftype" value = "oneway"/> One-Way Flight
	<input type="radio" onclick="DateDisable()" name = "ftype" value = "roundtrip"/> Round Trip Flight<br><br>
	<input type="checkbox" name = "isFlexible"/> Flexible Flight Dates (+- 3 Days)<br><br>
	<b>Where to? (3 Character Airport Code - i.e Newark = EWR)</b><br>
	From: <input type="text" name = "originAirport" maxlength = "3">
	To: <input type ="text" name = "destinationAirport" maxlength="3"><br>
	<b>When?</b><br>
	Departure Date (YYYY-MM-DD): <input type="text" name="deptDate">
	Arrival Date (YYYY-MM-DD): <input id="dateBtn" type="text" name="arrDate"><br><br>
	<input type="reset">
	<input type="submit" value="Search ->"/>
</form>
<br>
<script>
function DateEnable() {
	document.getElementbyId("dateBtn").removeAttribute("readOnly");
}
function DateDisable() {
	document.getElementbyId("dateBtn").setAttribute("readOnly",true);
}
</script>

</body>
</html>